######################################################
#                        BASE                        #
######################################################

FROM node:12.2.0-alpine AS base

ENV APP_PATH=/usr/src/app

WORKDIR ${APP_PATH}

######################################################
#                    DEVELOPMENT                     #
######################################################

FROM base AS development

COPY ./package.json .

RUN yarn install

COPY . .

CMD yarn dev