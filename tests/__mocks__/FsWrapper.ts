class FsWrapper {
  public name: string;

  constructor(name: string) {
    this.name = name;
  }

  public readSchema(path: string): any {
    /* tslint:disable:align */
    return `
component:
  name: Post
  schema:
    ObjectTypes:
      - name: User
        attributes:
          - name: _id
            type: String!
      - name: Post
        actions: ['c', 'r']
        attributes:
          - name: _id
            type: String!
            primaryKey: true
          - name: text
            type: String!
`;
  }
}

export default FsWrapper;
