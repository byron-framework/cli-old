import AST from 'src/ast/AST';

describe('AST', () => {
  beforeEach(() => {
    jest.resetAllMocks();
  });

  test('should build an AST', () => {
    const ast: AST = new AST('testing ast', 'namespace');

    expect(ast)
      .toBeDefined();
    expect(ast.name)
      .toBe('testing ast');
    expect(ast.namespace)
      .toBe('namespace');
  });

  test('should call accept', () => {
    const ast: AST = new AST('testing ast', 'namespace');
    const visitor: any = {
      visitAST: jest.fn(),
    };

    ast.accept(visitor);

    expect(visitor.visitAST)
      .toHaveBeenCalledTimes(1);
    expect(visitor.visitAST)
      .toHaveBeenCalledWith(ast);
  });

  test('should call visit to children', () => {
    const ast: AST = new AST('testing ast', 'namespace');

    const visitor: any = {
      visitAST: jest.fn(),
    };

    const child: any = {
      accept: jest.fn(),
    };

    ast.addChild(child);
    ast.accept(visitor);

    expect(child.accept).toHaveBeenCalledWith(visitor);
  });
});
