{{#importLines}}
{{{.}}}
{{/importLines}}

{{=__ __=}}

export interface __modelName__ extends Document {
  __#interfaceAttributes__
  __.__;
  __/interfaceAttributes__
}

export declare type T__modelName__ = Model<__modelName__>;

export const __modelName__Schema: Schema = new Schema({
  __#schemaAttributes__
  __.__,
  __/schemaAttributes__
}, {
  _id: false,
});

const __modelName__: T__modelName__ = model<__modelName__>('__modelName__', __modelName__Schema);

export default __modelName__;

__={{ }}=__