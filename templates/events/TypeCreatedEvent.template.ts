import Event from './Event';

{{=__ __=}}
class __type__CreatedEvent extends Event {
  constructor(entity: any) {
    super('New__type__Created', 'new.__subtopic__', entity);
  }
}

export default __type__CreatedEvent;
__={{ }}=__