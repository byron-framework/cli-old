import Event from './Event';

{{=__ __=}}
class __type__UpdatedEvent extends Event {
  constructor(entity: any) {
    super('__type__Updated', `__topicNamespace__.${entity._id}`, entity);
  }
}

export default __type__UpdatedEvent;

__={{ }}=__