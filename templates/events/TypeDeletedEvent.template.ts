import Event from './Event';

{{=__ __=}}
class __type__DeletedEvent extends Event {
  constructor(entity: any) {
    super('__type__Deleted', 'delete.__subtopic__', entity);
  }
}

export default __type__DeletedEvent;
__={{ }}=__