import { ApolloError } from 'apollo-server-koa';

{{=__ __=}}
async function get__type__(_parent: any, args: any, context: any, _info: any): Promise<any> {
  const { db: { __type__ } }: any = context;
  const {
    __#primaryKeys__
    __.__,
    __/primaryKeys__
  }: any = args;

  let __varName__ = await __type__.findOne({
    __#primaryKeys__
    __.__,
    __/primaryKeys__
  });

  if (!__varName__) {
    throw new ApolloError('__type__ not found', '409');
  }

  return __varName__;
}

export default get__type__;
__={{ }}=__
