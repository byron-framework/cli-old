import { ApolloError } from 'apollo-server-koa';

{{=__ __=}}
import __type__UpdatedEvent from 'src/events/__type__UpdatedEvent';

async function update__type__(
  _parent: any,
  args: any,
  context: any,
  _info: any,
): Promise<any> {
  const { db: { __type__}, stan }: any = context;
  const {
    __#primaryKeys__
    __.__,
    __/primaryKeys__
    __#args__
    __name__ __^last__,__/last__
    __/args__
  }: any = args;

  let __varName__ = await __type__.findOne({
    __#primaryKeys__
    __.__,
    __/primaryKeys__
  });

  if (!__varName__) {
    throw new ApolloError('Cannot find __type__', '409');
  }

  __varName__ = {
    ...__varName__,
    __#args__
    ...__name__ __^last__,__/last__
    __/args__
  };

  const event = new __type__UpdatedEvent(__varName__);
  stan.publish(event.topic, event.serialized());

  return __varName__;
}

export default update__type__;

__={{ }}=__
