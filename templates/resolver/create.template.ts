import { ApolloError } from 'apollo-server-koa';

{{=__ __=}}
import __type__CreatedEvent from '../events/__type__CreatedEvent';

async function create__type__(_parent: any, args: any, context: any, _info: any): Promise<any> {
  const { db: { __type__ }, stan, uuidv4 }: any = context;
  const {
    __#args__
    __name__ __^last__,__/last__
    __/args__
  }: any = args;

  let __varName__ = await __type__.findOne({
    __#args__
    ...__name__ __^last__,__/last__
    __/args__
  });

  if (__varName__) {
    throw new ApolloError('__type__ already exists', '409');
  }

  __varName__ = {
    _id: uuidv4(),
    __#args__
    ...__name__ __^last__,__/last__
    __/args__
  };

  const event = new __type__CreatedEvent(__varName__);
  stan.publish(event.topic, event.serialized());

  return __varName__;
}

export default create__type__;
__={{ }}=__
