import { ApolloError } from 'apollo-server-koa';

{{=__ __=}}
import __type__DeletedEvent from '../events/__type__DeletedEvent';

async function delete__type__(
  _parent: any,
  args: any,
  context: any,
  _info: any,
): Promise<any> {
  const { db: { __type__ }, stan }: any = context;
  const {
    __#primaryKeys__
    __.__,
    __/primaryKeys__
  }: any = args;

  let __varName__ = await __type__.findOne({
    __#primaryKeys__
    __.__,
    __/primaryKeys__
  });

  if (!__varName__) {
    throw new ApolloError('__type__ does not exist', '409');
  }

  const event = new __type__DeletedEvent(__varName__);
  stan.publish(event.topic, event.serialized());

  return __varName__;
}

export default delete__type__;
__={{ }}=__