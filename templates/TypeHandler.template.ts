import Handler from './Handler';

{{=__ __=}}
class __type__Handler extends Handler {
  constructor(stanContext: any, opts: any, db: any) {
    super(stanContext, opts, db, '__type__');
  }

  protected filterData(payload: any): any {
    return {
      __#filterAttributes__
      __.__: payload.__.__,
      __/filterAttributes__
    };
  }

  protected listenUpdates(_id: string): void {
    __#onlyId__
    return;
    __/onlyId__
    __^onlyId__
    const stan = this.stanContext.conn;

    const topic: string = `__topicNamespace__.${_id}`;
    this.stanContext.subs[topic] = stan
      .subscribe(topic, this.opts);

    this.stanContext.subs[topic]
      .on('message', async (msg: any): Promise<void> => {
        const { payload }: any = JSON
          .parse(msg.getData());
        
        await this.db[this.entityName]
          .updateOne({ _id }, payload);
      });

    this.stanContext.subs[topic]
      .on('unsubscribed', () => {
        console.log(`Unsubscribed from '__topicNamespace__.${_id}'`);
      });
    __/onlyId__
  }

  protected cancelUpdates(_id: string): void {
    __#onlyId__
    return;
    __/onlyId__
    __^onlyId__
    const topic: string = `__topicNamespace__.${_id}`;
    this.stanContext.subs[topic].unsubscribe();
    __/onlyId__
  }
}

export default __type__Handler;

__={{ }}=__