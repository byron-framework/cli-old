module.exports = {
  "testEnvironment": "node",
  "testRegex": "tests/.*\\.test\\.ts$",
  "roots": [
    "<rootDir>/src/",
    "<rootDir>/tests/"
  ],
  "modulePaths": [
    "."
  ],
  "transform": {
    "^.+\\.tsx?$": "ts-jest"
  },
  "testPathIgnorePatterns": [
    "/node_modules/",
    "./lib"
  ],
  "collectCoverageFrom": [
    "**/*.{js,jsx,ts,tsx}",
  ],
  "coverageReporters": [
    "lcov",
    "text",
    "json-summary"
  ],
  "coverageThreshold": {
    "global": {
      "statements": 3,
      "branches": 0,
      "functions": 3,
      "lines": 3,
    },
  },
}
