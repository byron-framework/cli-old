# Byron - Cloud-Native Reactive Microservices Framework
![npm version shield](https://img.shields.io/npm/v/@byronframework/cli.svg)
![npm license shield](https://img.shields.io/npm/l/@byronframework/cli.svg)
![gitlab build shield](https://img.shields.io/gitlab/pipeline/byron-framework/cli.svg)
![test coverage test](https://gitlab.com/byron-framework/cli/badges/match/coverage.svg?style=flat)


![twitter follow](https://img.shields.io/twitter/follow/byronframework.svg?label=Follow&style=social)

Byron is a framework to help you build an application that obeys the cloud-native and reactive principles while using a microservices archtecture.

## Documentation
Check out our [docs](https://byron.netlify.com/)

## Getting started
Check out our [guide](https://byron.netlify.com/guide/)

## License
[MIT](http://opensource.org/licenses/MIT)

Copyright (c) 2019-present, Leonardo Lana and João Daniel
