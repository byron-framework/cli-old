import { HttpLink } from 'apollo-link-http';
import { introspectSchema, makeRemoteExecutableSchema, addMockFunctionsToSchema, mergeSchemas } from 'graphql-tools';
import fetch from 'node-fetch';
import { GraphQLSchema } from 'graphql';

export async function createRemoteExecutableSchema(uri: string): Promise<any> {
  const link: HttpLink = new HttpLink({
    uri,
    fetch,
  });
  
  let schema: GraphQLSchema | null = null;

  try {
    schema = await introspectSchema(link)
  } catch (e) {
    console.log(`[${uri}] unreachable`);
  }

  if (schema === null) {
    return null;
  }

  const executableSchema = makeRemoteExecutableSchema({
    schema,
    link,
  });

  return executableSchema;
};

export function generateMergedSchema(rawSchemas: any[]): any {
  const schemas = rawSchemas
    .filter((schema: any): boolean => schema !== null);

  return mergeSchemas({ schemas });
};
