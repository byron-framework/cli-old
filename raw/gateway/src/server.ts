import Koa from 'koa';

import Cors from '@koa/cors';

import { ApolloServer } from 'apollo-server-koa';

import { generateMergedSchema, createRemoteExecutableSchema } from './utils';

const port: number = process.env.PORT ? +process.env.PORT : 3000;

const urls: string[] = Object.keys(process.env)
  .filter((env: string): boolean => env.match(/_service$/) !== null)
  .map((env: string): string => process.env[env] as string);

Promise
  .all(urls.map((url: string): any => createRemoteExecutableSchema(url)))
  .then((xSchemas: any) => {
    const gatewaySchema = generateMergedSchema(xSchemas);

    const server: ApolloServer = new ApolloServer({
      schema: gatewaySchema,
    });

    const app: Koa = new Koa();
    app.use(Cors());
    server.applyMiddleware({ app });

    app.listen({ port }, () => {
      console.log(`🚀 Server ready at http://localhost:${port}${server.graphqlPath}`);
    });
  });
