# Guide

A guide to get started with Byron.

## Installation

Firstly install `Byron` CLI:

```bash
$ npm install --global @byronframework/cli
```

Also, make sure you have `Docker` and `docker-compose` installed. But if you don't, here're the links:
- Docker [installation guide](https://docs.docker.com/install/)
- docker-compose [installation guide](https://docs.docker.com/compose/install/)

## Our system
We are going to build JunoWeb, a system that manages students enrollments in various classes.

## First Component

To create a boilerplate, run:

```bash
$ byron init Student
```

This will generate a `StudentComponent` directory, with a `schema.yml` inside.

Now, open the `./StudentComponent/schema.yml` and insert the following `yml`

```yml
name: Student
namespace: JunoWeb

schema:
  ObjectTypes:
  - name: Student
    actions: ['c', 'r', 'u', 'd']
    attributes:
    - name: name
      type: String!
    - name: email
      type: String!
    - name: course
      type: String!
```

This `yml` file will create a Byron component with `Student` as its name, belonging at the `JunoWeb` namespace.
The `schema` key will help you bootstrap the logic of your component, this `schema` will create a `Student` GraphQL type,
its `mongoose` model. With the `actions` value being `['c', 'r', 'u', 'd']`, Byron will generate CRUD resolvers for the
`Student` type.

Now to generate all of this, run:

```
$ byron build StudentComponent
```

All generated files will be at `/tmp/JunoWeb/StudentComponent`

## Second Component

Create the second component by running:

```bash
$ byron init Enrollment
```

Edit the `./EnrollmentComponent/schema.yml`:

```yml
name: Enrollment
namespace: JunoWeb

schema:
  ObjectTypes:
  - name: Student
  - name: Discipline
    actions: ['c','r', 'u']
    attributes:
    - name: name
      type: String!
    - name: code
      type: String!
  - name: Enrollment
    actions: ['c', 'r', 'd']
    attributes:
    - name: discipline_id
      type: String!
    - name: student_id
      type: String!

config:
  apiPort: 4010
  sinkPort: 4011
```

This component will have the same types as the first one, but will also have a `config` key that configures the component
in a infrastructure level. This component will declare a `Student` type so it can listen to the `Student` events emitted by
our first component.

> If not set, apiPort and sinkPort will be set to `4000` to `4000` respectively.

Now, build this component too:
```bash
$ byron build EnrollmentComponent
```

## Running JunoWeb

To run `JunoWeb`, go to the build directorie
```bash
$ cd /tmp/JunoWeb
```

And run the `run.sh` script:
```bash
$ bash ./run.sh
```

And finally visit `http://localhost:3000/graphql`, see the system online and test it.

## Next Steps
For more information visit the [Schema API reference](../api/README.md)
