module.exports = {
  title: 'Byron',
  description: 'Cloud-Native Reactive Microservices Framework',
  themeConfig: {
    logo: '/images/logo.jpg',
    repo: 'https://gitlab.com/byron-framework/cli',
    sidebar: 'auto',
    nav: [
      { text: 'Home', link: '/' },
      { text: 'Guide', link: '/guide/' },
      { text: 'CLI', link: '/cli/' },
      { text: 'Schema API', link: '/api/' },
    ]
  }
};
