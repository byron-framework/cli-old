import { copySync, existsSync, mkdirSync, readFileSync, writeFileSync } from 'fs-extra';
import { resolve } from 'path';

class FsWrapper {
  private static _instance: FsWrapper;

  private rootPath: string;
  private buildPath: string;
  private buildRootPath: string;
  private componentPath: string;

  private constructor() {
    this.buildPath = '/tmp';
    this.buildRootPath = '/tmp';

    this.componentPath = '.';

    this.rootPath = resolve(__dirname, '..') + '/';
  }

  public static get instance(): FsWrapper {
    if (!FsWrapper._instance) {
      FsWrapper._instance = new FsWrapper();
    }

    return FsWrapper._instance;
  }

  public copyRawComponent(componentName: string, namespace: string, componentPath: string): void {
    this.buildPath = namespace
      ? `/tmp/${namespace}/${componentName}Component`
      : `/tmp/${componentName}Component`;

    this.buildRootPath = namespace
      ? `/tmp/${namespace}`
      : `/tmp`;

    this.componentPath = componentPath;

    this.copy(this.rootPath + 'raw/component', this.buildPath);
  }

  public copyGateway(): void {
    this.copy(this.rootPath + 'raw/gateway', this.buildRootPath + '/Gateway');
  }

  public copyScripts(): void {
    this.copy(this.rootPath + 'raw/shellScripts', this.buildRootPath);
  }

  public copyResolvers(): void {
    this.copy(
      resolve(this.componentPath, 'resolvers'),
      resolve(this.buildPath, 'api', 'src', 'resolvers'),
    );
  }

  public readSchema(path: string): any {
    return this.read(resolve(path, 'schema.yml'));
  }

  public readTemplate(name: string): string {
    return this.read(this.rootPath + `templates/${name}`);
  }

  public readGatewayDockerCompose(): string {
    if (!existsSync(`${this.buildRootPath}/Gateway/docker-compose.yaml`)) {
      return this.readTemplate('docker-compose/gateway.template.yaml');
    }

    return this.read(`${this.buildRootPath}/Gateway/docker-compose.yaml`);
  }

  public readBuiltGQLSchema(): string {
    return this.read(resolve(this.buildPath, 'api', 'src', 'graphql', 'schema.graphql'));
  }

  public writeGraphQLSchema(schema: string): void {
    const dirName: string = `${this.buildPath}/api/src/graphql`;

    if (!existsSync(dirName)) {
      mkdirSync(dirName);
    }

    const path: string = `${dirName}/schema.graphql`;
    this.write(path, schema);
  }

  public writeModel(modelName: string, model: string): void {
    const contexts: string[] = ['api', 'sink'];

    contexts.forEach((ctx: string): void => {
      const path: string = `${this.buildPath}/${ctx}/src/db/models/${modelName}.ts`;
      this.write(path, model);
    });
  }

  public writeResolver(resolverName: string, resolver: string): void {
    const path: string = `${this.buildPath}/api/src/resolvers/${resolverName}.ts`;
    this.write(path, resolver);
  }

  public writeEvent(eventName: string, event: string): void {
    const path: string = `${this.buildPath}/api/src/events/${eventName}.ts`;
    this.write(path, event);
  }

  public writeHandler(handlerName: string, handler: string): void {
    const dirName: string = `${this.buildPath}/sink/src/handlers`;

    if (!existsSync(dirName)) {
      mkdirSync(dirName);
    }

    const path: string = `${dirName}/${handlerName}.ts`;
    this.write(path, handler);
  }

  public writeComponentDockerCompose(dockerCompose: string): void {
    const path: string = `${this.buildPath}/docker-compose.yaml`;
    this.write(path, dockerCompose);
  }

  public writeBrokerDockerCompose(): void {
    const path: string = `${this.buildRootPath}/docker-compose.yaml`;
    const dockerCompose: string = this.readTemplate('docker-compose/broker.template.yaml');
    this.write(path, dockerCompose);
  }

  public writeGatewayDockerCompose(dockerCompose: string): void {
    const path: string = `${this.buildRootPath}/Gateway/docker-compose.yaml`;
    this.write(path, dockerCompose);
  }

  private copy(src: string, dest: string): void {
    copySync(src, dest);
  }

  private read(path: string): string {
    return readFileSync(path)
      .toString();
  }

  private write(path: string, content: string): void {
    writeFileSync(path, content);
    console.log('\x1b[32m%s\x1b[32m', `\t+ ${path}`);
  }
}

export default FsWrapper;
