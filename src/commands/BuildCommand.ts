import Command from 'src/commands/Command';
import HelpMessage from 'src/commands/HelpMessage';

import Mustache from 'mustache';

import AST from 'src/ast/AST';

import FsWrapper from 'src/FsWrapper';

import YAMLInterpreter from 'src/interpreters/YAMLInterpreter';

import GeneratorPipeline from 'src/generators/GeneratorPipeline';

import ActionsResolversGenerator from 'src/generators/ActionsResolversGenerator';
import DockerComposeGenerator from 'src/generators/DockerComposeGenerator';
import EventsGenerator from 'src/generators/EventsGenerator';
import GatewayGenerator from 'src/generators/GatewayGenerator';
import GraphQLSchemaGenerator from 'src/generators/GraphQLSchemaGenerator';
import HandlersGenerator from 'src/generators/HandlersGenerator';
import ModelsGenerator from 'src/generators/ModelsGenerator';
import ResolverGenerator from 'src/generators/ResolversGenerator';

Mustache.escape = (text: string): string => text;

class BuildCommand extends Command {
  public static help(): HelpMessage {
    return {
      command: 'build',
      description: 'Build a component from schema',
    };
  }

  private static _commandName: string = 'build';

  public static get commandName(): string {
    return this._commandName;
  }

  private pathToSchema: string;

  constructor(pathToSchema: string) {
    super();

    this.pathToSchema = pathToSchema;
  }

  public run(): void {
    const yamlInterpreter: YAMLInterpreter =
      new YAMLInterpreter(this.pathToSchema);

    const ast: AST = yamlInterpreter.buildAST();

    this.scaffold(ast.name, ast.namespace);

    const pipeline: GeneratorPipeline = new GeneratorPipeline(ast);

    // TODO: add ErrorGenerator (Validation of the AST?)
    pipeline
      .add(new GraphQLSchemaGenerator())
      .add(new ModelsGenerator())
      .add(new EventsGenerator())
      .add(new ActionsResolversGenerator())
      .add(new HandlersGenerator())
      .add(new DockerComposeGenerator())
      .add(new GatewayGenerator())
      .add(new ResolverGenerator())
      .run();
  }

  private scaffold(component: string, namespace: string): void {
    const fs: FsWrapper = FsWrapper.instance;
    fs.copyRawComponent(component, namespace, this.pathToSchema);
    fs.copyGateway();
    fs.copyScripts();
    fs.writeBrokerDockerCompose();
  }
}

export default BuildCommand;
