interface HelpMessage {
  command: string;
  description: string;
}

export default HelpMessage;
