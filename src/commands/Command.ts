abstract class Command {
  public abstract run(): void;
}

export default Command;
