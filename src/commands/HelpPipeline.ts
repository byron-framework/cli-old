import HelpMessage from 'src/commands/HelpMessage';

import figlet from 'figlet';

class HelpPipeline {
  private messages: HelpMessage[];
  private defaultTab: string = ' '.repeat(2);
  private defaultSpacing: number = 12;

  constructor() {
    this.messages = [];
  }

  public add(message: HelpMessage): HelpPipeline {
    this.messages.push(message);

    return this;
  }

  public print(): void {
    console.log(
      figlet.textSync('Byron'),
    );

    console.log('\nUsage: byron COMMAND\n');
    console.log('A cloud-native reactive microservices framework\n');

    console.log('Management Commands:');
    console.log(`${this.defaultTab}component${this.spacing('component')}Manage components`);
    console.log('\nCommands:');

    this.messages.forEach((msg: HelpMessage) => {
        const spacing: string = this.spacing(msg.command);
        console.log(
          `${this.defaultTab}%s${spacing}%s`,
          msg.command,
          msg.description,
        );
      });

    console.log('\nRun \'byron help COMMAND\' for more information on a command');
  }

  private spacing(word: string): string {
    const numberOfSpaces: number = this.defaultSpacing - word.length;
    return ' '.repeat(numberOfSpaces);
  }
}

export default HelpPipeline;
