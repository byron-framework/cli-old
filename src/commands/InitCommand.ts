import Command from 'src/commands/Command';
import HelpMessage from 'src/commands/HelpMessage';

import fs from 'fs-extra';
import { render } from 'mustache';
import { resolve } from 'path';

import FsWrapper from 'src/FsWrapper';

class InitCommand extends Command {
  public static help(): HelpMessage {
    return {
      command: 'init',
      description: 'Initialize a Byron component boilerplate',
    };
  }

  private static _commandName: string = 'init';

  public static get commandName(): string {
    return this._commandName;
  }

  private componentName: string;
  private componentPath: string;

  constructor(name: string, path: string = '.') {
    super();

    this.componentName =
      name.slice(0, 1)
        .toUpperCase() +
      name.slice(1)
        .toLowerCase();

    this.componentPath = path;
  }

  public run(): void {
    const path: string = resolve(this.componentPath, `${this.componentName}Component`);

    if (fs.existsSync(path)) {
      throw new Error('Directory already exists');
    }

    const template: string = FsWrapper
      .instance
      .readTemplate('package.template.json');

    const packageJson: string = render(template, {
      name: this.componentName,
    });

    fs.mkdirSync(path);
    fs.mkdirSync(resolve(path, 'resolvers'));
    fs.mkdirSync(resolve(path, 'hooks'));
    fs.mkdirSync(resolve(path, 'handlers'));
    fs.writeFile(resolve(path, 'schema.yml'), '');
    fs.writeFile(resolve(path, 'package.json'), packageJson);
  }
}

export default InitCommand;
