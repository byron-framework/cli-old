import BuildCommand from 'src/commands/BuildCommand';
import InitCommand from 'src/commands/InitCommand';

import HelpPipeline from 'src/commands/HelpPipeline';

const [ command, ...args ]: string[] = process.argv.slice(2);

switch (command) {
  case 'init':
    const [ name, initPath ]: string[] = args;
    new InitCommand(name, initPath).run();
    break;
  case 'build':
    const [ path ]: string[] = args;
    console.log(path);
    new BuildCommand(path).run();
    break;
  case 'help':
    const pipeline: HelpPipeline = new HelpPipeline();
    pipeline
      .add(InitCommand.help())
      .add(BuildCommand.help());
    pipeline.print();

}
