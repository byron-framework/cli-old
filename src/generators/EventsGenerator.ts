import { render } from 'mustache';

import NodeGenerator from 'src/generators/NodeGenerator';

import FsWrapper from 'src/FsWrapper';

import AST from 'src/ast/AST';

import Argument from 'src/ast/Argument';
import Attribute from 'src/ast/Attribute';
import Config from 'src/ast/Config';
import ObjectType from 'src/ast/ObjectType';
import Resolver from 'src/ast/Resolver';

class EventsGenerator extends NodeGenerator {
  private types: { [key: string]: string[] };

  constructor() {
    super();
    this.types = {};
  }

  public visitAST(_: AST): void {
    Object.keys(this.types)
      .filter((key: string): boolean => this.types[key].includes('c'))
      .forEach((key: string): void => {
        const view: any = {
          type: key,
          subtopic: key.toLowerCase(),
        };

        const template: string = FsWrapper.instance
          .readTemplate('events/TypeCreatedEvent.template.ts');

        FsWrapper.instance
          .writeEvent(`${key}CreatedEvent`, render(template, view));
      });

    Object.keys(this.types)
      .filter((key: string): boolean => this.types[key].includes('u'))
      .forEach((key: string): void => {
        const view: any = {
          type: key,
          topicNamespace: key.toLowerCase(),
        };

        const template: string = FsWrapper.instance
          .readTemplate('events/TypeUpdatedEvent.template.ts');

        FsWrapper.instance
          .writeEvent(`${key}UpdatedEvent`, render(template, view));
      });

    Object.keys(this.types)
      .filter((key: string): boolean => this.types[key].includes('d'))
      .forEach((key: string): void => {
        const view: any = {
          type: key,
          subtopic: key.toLowerCase(),
        };

        const template: string = FsWrapper.instance
          .readTemplate('events/TypeDeletedEvent.template.ts');

        FsWrapper.instance
          .writeEvent(`${key}DeletedEvent`, render(template, view));
      });
  }

  public visitObjectType(ot: ObjectType): void {
    this.types[ot.name] = [];
    this.types[ot.name] = ot.actions;
  }

  public visitAttribute(_: Attribute): void {
    return;
  }

  public visitConfig(_: Config): void {
    return;
  }

  public visitResolver(_: Resolver): void {
    return;
  }

  public visitArgument(_: Argument): void {
    return;
  }
}

export default EventsGenerator;
