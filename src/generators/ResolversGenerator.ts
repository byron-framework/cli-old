import { render } from 'mustache';

import FsWrapper from 'src/FsWrapper';

import NodeGenerator from 'src/generators/NodeGenerator';

import AST from 'src/ast/AST';

import Argument from 'src/ast/Argument';
import Attribute from 'src/ast/Attribute';
import Config from 'src/ast/Config';
import ObjectType from 'src/ast/ObjectType';
import Resolver from 'src/ast/Resolver';

interface ArgumentData {
  name: string;
  type: string;
  last: boolean;
}

interface ResolverData {
  return: string;
  type: string;
  args: ArgumentData[];
}

interface ResolverView {
  name: string;
  args: ArgumentData[];
  return: string;
  noArgs: boolean;
}

class ResolverGenerator extends NodeGenerator {
  private resolvers: { [key: string]: ResolverData };

  constructor() {
    super();

    this.resolvers = {};
  }

  public visitAST(_: AST): void {
    const fs: FsWrapper = FsWrapper.instance;

    const template: string = fs.readTemplate('resolver.template.graphql');

    const viewStrings: { [key: string]: string[] } = {
      query: [],
      mutation: [],
    };

    Object.keys(this.resolvers)
      .forEach((key: string): void => {
        const rsl: ResolverData = this.resolvers[key];

        const view: ResolverView = {
          name: key,
          noArgs: rsl.args.length > 0,
          ...rsl,
        };

        if (view.args.length) {
          view.args[view.args.length - 1].last = true;
        }

        viewStrings[rsl.type].push(render(template, view));
      });

    fs.copyResolvers();
  }

  public visitObjectType(_: ObjectType): void {
    return;
  }

  public visitAttribute(_: Attribute): void {
    return;
  }

  public visitConfig(_: Config): void {
    return;
  }

  public visitResolver(resolver: Resolver): void {
    this.resolvers[resolver.name] = {
      return: resolver.return,
      type: resolver.type,
      args: [],
    };
  }

  public visitArgument(arg: Argument): void {
    const key: string = arg.parent.name;
    this.resolvers[key].args.push({
      name: arg.name,
      type: arg.type,
      last: false,
    });
  }
}

export default ResolverGenerator;
