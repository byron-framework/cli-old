import { render } from 'mustache';

import FsWrapper from 'src/FsWrapper';

import NodeGenerator from 'src/generators/NodeGenerator';

import AST from 'src/ast/AST';

import Argument from 'src/ast/Argument';
import Attribute from 'src/ast/Attribute';
import Config from 'src/ast/Config';
import ObjectType from 'src/ast/ObjectType';
import Resolver from 'src/ast/Resolver';

interface LocalAttr {
  name: string;
  type: string;
  isPrimitive: boolean;
  isRequired: boolean;
}

class ModelsGenerator extends NodeGenerator {
  private readonly primitiveTypes: { [key: string]: { [key: string]: string } } = {
    ID: { ts: 'string', js: 'String' },
    String: { ts: 'string', js: 'String' },
    Int: { ts: 'number', js: 'Number' },
    Float: { ts: 'number', js: 'Number' },
    Boolean: { ts: 'boolean', js: 'Boolean' },
  };

  private models: { [key: string]: {
    attrs: LocalAttr[],
    importLines: string[],
  } };

  constructor() {
    super();
    this.models = {};
  }

  public visitAST(_: AST): void {
    Object.keys(this.models)
      .forEach((modelName: string): void => this.generateFile(modelName));
  }

  public visitObjectType(ot: ObjectType): void {
    this.models[ot.name] = {
      attrs: [],
      importLines: [`import { Document, model, Model, Schema } from 'mongoose';`],
    };
  }

  public visitAttribute(attr: Attribute): void {
    const isRequired: boolean = attr.type.includes('!');

    const attrType: string = isRequired
      ? attr.type.slice(0, -1)
      : attr.type;

    const isPrimitive: boolean =
      Object
        .keys(this.primitiveTypes)
        .includes(attrType);

    if (!isPrimitive) {
      this.models[attr.parent.name].importLines
        .push(`import { ${attrType}, ${attrType}Schema } from './${attrType}';`);
    }

    this.models[attr.parent.name].attrs.push({
      name: attr.name,
      type: attrType,
      isPrimitive,
      isRequired,
    });
  }

  public visitConfig(_: Config): void {
    return;
  }

  public visitResolver(_: Resolver): void {
    return;
  }

  public visitArgument(_: Argument): void {
    return;
  }

  private interfaceModelAttributes(localAttrs: LocalAttr[]): string[] {
    return localAttrs
      .map((lAttr: LocalAttr): string => {
        const name: string = lAttr.isRequired
          ? lAttr.name
          : lAttr.name + '?';

        const type: string = lAttr.isPrimitive
          ? this.primitiveTypes[lAttr.type].ts
          : lAttr.type;

        return `${name}: ${type}`;
      });
  }

  private schemaModelAttributes(localAttrs: LocalAttr[]): string[] {
    return localAttrs
      .map((lAttr: LocalAttr): string => {
        const type: string = lAttr.isPrimitive
          ? this.primitiveTypes[lAttr.type].js
          : lAttr.type + 'Schema';

        return `${lAttr.name}: ${type}`;
      });
  }

  private importLines(modelName: string): string[] {
    return this.models[modelName].importLines;
  }

  private generateFile(modelName: string): void {
    const template: string = FsWrapper.instance
      .readTemplate('model.template.ts');

    const view: any = {
      importLines: this.importLines(modelName),
      modelName,
      interfaceAttributes: this.interfaceModelAttributes(this.models[modelName].attrs),
      schemaAttributes: this.schemaModelAttributes(this.models[modelName].attrs),
    };

    FsWrapper.instance
      .writeModel(modelName, (render(template, view)));
  }
}

export default ModelsGenerator;
