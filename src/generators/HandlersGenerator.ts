import { render } from 'mustache';

import FsWrapper from 'src/FsWrapper';

import NodeGenerator from 'src/generators/NodeGenerator';

import AST from 'src/ast/AST';

import Argument from 'src/ast/Argument';
import Attribute from 'src/ast/Attribute';
import Config from 'src/ast/Config';
import ObjectType from 'src/ast/ObjectType';
import Resolver from 'src/ast/Resolver';

interface TemplateTuple {
  filename: string;
  view: TemplateView;
}

interface TemplateView {
  filterAttributes: string[];
  topicNamespace: string;
  type: string;
  onlyId: boolean;
}

class HandlersGenerator extends NodeGenerator {
  private handlers: TemplateTuple[];

  constructor() {
    super();
    this.handlers = [];
  }

  public visitAST(_: AST): void {
    const fs: FsWrapper = FsWrapper.instance;
    const template: string = fs.readTemplate('TypeHandler.template.ts');

    this.handlers
      .forEach((handler: TemplateTuple): void => {
        handler.view.onlyId = handler.view.filterAttributes.length === 1;

        const content: string = render(template, handler.view);

        fs.writeHandler(`${handler.filename}Handler`, content);
      });
  }

  public visitObjectType(ot: ObjectType): void {
    this.handlers.push({
      filename: ot.name,
      view: {
        filterAttributes: [],
        topicNamespace: ot.name.toLowerCase(),
        type: ot.name,
        onlyId: true,
      },
    });
  }

  public visitAttribute(attr: Attribute): void {
    const tuple: TemplateTuple | undefined = this.handlers
      .find((t: TemplateTuple): boolean => t.filename === attr.parent.name);

    (tuple as TemplateTuple).view.filterAttributes.push(attr.name);
  }

  public visitConfig(_: Config): void {
    return;
  }

  public visitResolver(_: Resolver): void {
    return;
  }

  public visitArgument(_: Argument): void {
    return;
  }
}

export default HandlersGenerator;
