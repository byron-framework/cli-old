import AST from 'src/ast/AST';

import Argument from 'src/ast/Argument';
import Attribute from 'src/ast/Attribute';
import Config from 'src/ast/Config';
import ObjectType from 'src/ast/ObjectType';
import Resolver from 'src/ast/Resolver';

abstract class NodeGenerator {
  public abstract visitAST(ast: AST): any;

  public abstract visitObjectType(ot: ObjectType): any;

  public abstract visitAttribute(attr: Attribute): any;

  public abstract visitConfig(config: Config): any;

  public abstract visitResolver(resolver: Resolver): any;

  public abstract visitArgument(argument: Argument): any;
}

export default NodeGenerator;
