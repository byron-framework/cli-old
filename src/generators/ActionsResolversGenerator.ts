import { render } from 'mustache';

import FsWrapper from 'src/FsWrapper';

import NodeGenerator from 'src/generators/NodeGenerator';

import AST from 'src/ast/AST';

import Argument from 'src/ast/Argument';
import Attribute from 'src/ast/Attribute';
import Config from 'src/ast/Config';
import ObjectType from 'src/ast/ObjectType';
import Resolver from 'src/ast/Resolver';

interface CreationView {
  type: string;
  varName: string;
  args: [{
    name: string;
    last: boolean;
  }];
}

interface ReadOneView {
  type: string;
  varName: string;
  primaryKeys: string[];
}

interface DeletionView {
  type: string;
  primaryKeys: string[];
  varName: string;
  [key: string]: any;
}

interface UpdateView {
  primaryKeys: string[];
  args: [{
    name: string;
    last: boolean;
  }];
  type: string;
  varName: string;
  [key: string]: any;
}

class ActionsResolversGenerator extends NodeGenerator {
  private types: {
    [key: string]: {
      attrs: Attribute[];
      creationView?: CreationView;
      readOneView?: ReadOneView;
      deletionView?: DeletionView;
      updateView?: UpdateView;
    };
  };

  constructor() {
    super();
    this.types = {};
  }

  public visitAST(_: AST): void {
    const fs: FsWrapper = FsWrapper.instance;

    const createTemplate: string = fs.readTemplate('resolver/create.template.ts');
    const readOneTemplate: string = fs.readTemplate('resolver/get.template.ts');
    const updateTemplate: string = fs.readTemplate('resolver/update.template.ts');
    const deleteTemplate: string = fs.readTemplate('resolver/delete.template.ts');

    Object.keys(this.types)
      .forEach((key: string): void => {
        if (this.types[key].creationView) {
          const createResolver: string = render(createTemplate, this.types[key].creationView);
          fs.writeResolver(`create${key}`, createResolver);
        }

        if (this.types[key].readOneView) {
          const readOneResolver: string = render(readOneTemplate, this.types[key].readOneView);
          fs.writeResolver(`get${key}`, readOneResolver);
        }

        if (this.types[key].updateView) {
          const updateResolver: string = render(updateTemplate, this.types[key].updateView);
          fs.writeResolver(`update${key}`, updateResolver);
        }

        if (this.types[key].deletionView) {
          const deleteResolver: string = render(deleteTemplate, this.types[key].deletionView);
          fs.writeResolver(`delete${key}`, deleteResolver);
        }
      });
  }

  public visitObjectType(ot: ObjectType): void {
    if (ot.actions.length) {
      this.types[ot.name] = {
        attrs: [],
      };
    }

    if (ot.actions.includes('c')) {
      this.types[ot.name].creationView = {
        type: ot.name,
        args: [{
          name: 'data',
          last: true,
        }],
        varName: ot.name.toLowerCase(),
      };
    }

    if (ot.actions.includes('r')) {
      this.types[ot.name].readOneView = {
        type: ot.name,
        primaryKeys: ['_id'],
        varName: ot.name.toLowerCase(),
      };
    }

    if (ot.actions.includes('u')) {
      this.types[ot.name].updateView = {
        type: ot.name,
        primaryKeys: ['_id'],
        args: [{
          name: 'data',
          last: true,
        }],
        varName: ot.name.toLowerCase(),
      };
    }

    if (ot.actions.includes('d')) {
      this.types[ot.name].deletionView = {
        type: ot.name,
        primaryKeys: ['_id'],
        varName: ot.name.toLowerCase(),
      };
    }
  }

  public visitAttribute(attr: Attribute): void {
    if (this.types[attr.parent.name]) {
      this.types[attr.parent.name].attrs.push(attr);
    }
  }

  public visitConfig(_: Config): void {
    return;
  }

  public visitResolver(_: Resolver): void {
    return;
  }

  public visitArgument(_: Argument): void {
    return;
  }
}

export default ActionsResolversGenerator;
