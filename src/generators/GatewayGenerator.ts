import NodeGenerator from 'src/generators/NodeGenerator';

import FsWrapper from 'src/FsWrapper';

import AST from 'src/ast/AST';

import Argument from 'src/ast/Argument';
import Attribute from 'src/ast/Attribute';
import Config from 'src/ast/Config';
import ObjectType from 'src/ast/ObjectType';
import Resolver from 'src/ast/Resolver';

import { safeDump, safeLoad } from 'js-yaml';

class Gateway extends NodeGenerator {
  private componentApiPort: number;
  private dcYAML: { [key: string]: any };
  private services: {
    [key: string]: string;
  };

  constructor() {
    super();
    this.componentApiPort = 4000;
    this.services = {};

    this.dcYAML = safeLoad(
      FsWrapper
        .instance
        .readGatewayDockerCompose(),
    );

    const gatewayEnv: { [key: string]: any } = this.dcYAML.services.gateway.environment;
    const serviceRegex: RegExp = /[A-z]+_service/g;

    Object.keys(gatewayEnv)
      .forEach((key: string) => {
        if (!serviceRegex.exec(key)) {
          return;
        }

        this.services[key] = gatewayEnv[key];
      });
  }

  public visitAST(ast: AST): void {
    const componentName: string = ast.name.toLowerCase();
    const serviceName: string = componentName + '_service';
    const serviceAddress: string = `http://api_${componentName}:${this.componentApiPort}/graphql`;

    this.services[serviceName] = serviceAddress;

    this.dcYAML.services.gateway.environment = {
      ...this.dcYAML.services.gateway.environment,
      ...this.services,
    };

    FsWrapper
      .instance
      .writeGatewayDockerCompose(
        safeDump(this.dcYAML),
      );
  }

  public visitObjectType(_: ObjectType): void {
    return;
  }

  public visitAttribute(_: Attribute): void {
    return;
  }

  public visitConfig(config: Config): void {
    this.componentApiPort = config.apiPort;
  }

  public visitResolver(_: Resolver): void {
    return;
  }

  public visitArgument(_: Argument): void {
    return;
  }
}

export default Gateway;
