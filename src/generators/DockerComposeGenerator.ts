import NodeGenerator from 'src/generators/NodeGenerator';

import { render } from 'mustache';

import FsWrapper from 'src/FsWrapper';

import AST from 'src/ast/AST';

import Argument from 'src/ast/Argument';
import Attribute from 'src/ast/Attribute';
import Config from 'src/ast/Config';
import ObjectType from 'src/ast/ObjectType';
import Resolver from 'src/ast/Resolver';

class DockerComposeGenerator extends NodeGenerator {
  private apiPort: number = 4000;
  private sinkPort: number = 4001;

  constructor() {
    super();
  }

  public visitAST(ast: AST): void {
    const template: string = FsWrapper
      .instance
      .readTemplate('docker-compose/component.template.yaml');

    const view: { [key: string]: string | number } = {
      typeName: ast.name,
      serviceName: ast.name.toLowerCase(),
      apiPort: this.apiPort,
      sinkPort: this.sinkPort,
    };

    FsWrapper
      .instance
      .writeComponentDockerCompose(render(template, view));
  }

  public visitObjectType(_: ObjectType): void {
    return;
  }

  public visitAttribute(_: Attribute): void {
    return;
  }

  public visitConfig(config: Config): void {
    this.apiPort = config.apiPort;
    this.sinkPort = config.sinkPort;
  }

  public visitResolver(_: Resolver): void {
    return;
  }

  public visitArgument(_: Argument): void {
    return;
  }
}

export default DockerComposeGenerator;
