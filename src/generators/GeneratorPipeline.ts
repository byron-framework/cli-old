import AST from 'src/ast/AST';

import NodeGenerator from './NodeGenerator';

class GeneratorPipeline {
  private generators: NodeGenerator[];

  constructor(
    private ast: AST,
  ) {
    this.generators = [];
  }

  public add(generator: NodeGenerator): GeneratorPipeline {
    this.generators.push(generator);

    return this;
  }

  public run(): void {
    this.generators
      .forEach((gen: NodeGenerator): void => this.ast.accept(gen));
  }
}

export default GeneratorPipeline;
