import FsWrapper from 'src/FsWrapper';

import NodeGenerator from 'src/generators/NodeGenerator';

import AST from 'src/ast/AST';

import Argument from 'src/ast/Argument';
import Attribute from 'src/ast/Attribute';
import Config from 'src/ast/Config';
import ObjectType from 'src/ast/ObjectType';
import Resolver from 'src/ast/Resolver';

import { render } from 'mustache';

interface GQLFunction {
  name: string;
  arguments: [{
    name: string;
    type: string;
    last: boolean;
  }];
  returnType?: string;
}

interface GQLAttribute {
  name: string;
  type: string;
}

interface GQLType {
  name: string;
  attributes: GQLAttribute[];
}

interface GQLObjectType extends GQLType {
  actions: string[];
}

interface TemplateView {
  hasTypes: boolean;
  hasInputs: boolean;
  hasMutations: boolean;
  hasQueries: boolean;

  types: GQLType[];
  inputs: GQLType[];
  queries: GQLFunction[];
  mutations: GQLFunction[];
}

class GraphQLSchemaGenerator extends NodeGenerator {
  private view: TemplateView;
  private types: { [key: string]: GQLObjectType };

  constructor() {
    super();

    this.view = {
      hasTypes: false,
      hasInputs: false,
      hasMutations: false,
      hasQueries: false,
      types: [],
      inputs: [],
      queries: [],
      mutations: [],
    };

    this.types = {};
  }

  public visitAST(_: AST): void {
    const fs: FsWrapper = FsWrapper.instance;

    this.buildView();
    const template: string = fs.readTemplate('schema.template.graphql');
    const schema: string = render(template, this.view);

    FsWrapper.instance.writeGraphQLSchema(schema);
  }

  public visitObjectType(ot: ObjectType): void {
    this.view.hasTypes = true;

    this.types[ot.name] = {
      name: ot.name,
      attributes: [],
      actions: ot.actions,
    };
  }

  public visitAttribute(attr: Attribute): void {
    const givenType: GQLObjectType = this.types[attr.parent.name];

    if (!givenType.attributes) {
      givenType.attributes = [];
    }

    const { name, type }: Attribute = attr;

    givenType.attributes.push({
      name,
      type,
    });
  }

  public visitConfig(_: Config): void {
    return;
  }

  public visitResolver(resolver: Resolver): void {
    if (resolver.type === 'query') {
      this.view.hasQueries = true;

      const args: any = (resolver.children as Argument[])
        .map((arg: Argument): any => {
          const { name, type }: Argument = arg;

          return {
            name,
            type,
            last: false,
          };
        });

      if (args.length) {
        args[args.length - 1].last = true;
      }

      this.view.queries.push({
        name: resolver.name,
        arguments: args,
        returnType: resolver.return,
      });
    }
  }

  public visitArgument(_: Argument): void {
    return;
  }

  private buildView(): void {
    this.view.types = Object.keys(this.types)
      .map((name: string): GQLType => {
        const type: GQLObjectType = this.types[name];

        this.view.hasMutations = this.view.hasMutations ||
          ['c', 'u', 'd'].some((action: string) => {
            return type.actions.includes(action);
          });

        this.view.hasInputs = this.view.hasInputs ||
          ['c', 'u'].some((action: string) => {
            return type.actions.includes(action);
          });

        if (type.actions.includes('c')) {
          this.buildCreate(type);
        }

        if (type.actions.includes('r')) {
          this.buildReadOne(type);
        }

        if (type.actions.includes('u')) {
          this.buildUpdate(type);
        }

        if (type.actions.includes('d')) {
          this.buildDelete(type);
        }

        return {
          name,
          ...type,
        };
      });
  }

  private buildCreate(ot: GQLObjectType): void {
    const inputAttributes: GQLAttribute[] = ot.attributes
      .filter((attr: GQLAttribute): boolean => attr.name !== '_id');

    this.view.inputs.push({
      name: `create${ot.name}Input`,
      attributes: inputAttributes,
    });

    const args: any = [{
      name: 'data',
      type: `create${ot.name}Input!`,
      last: true,
    }];

    this.view.mutations.push({
      name: `create${ot.name}`,
      arguments: args,
      returnType: `${ot.name}!`,
    });
  }

  private buildReadOne(ot: GQLObjectType): void {
    this.view.hasQueries = true;

    const args: any = [{
      name: '_id',
      type: 'ID!',
      last: true,
    }];

    this.view.queries.push({
      name: `get${ot.name}`,
      arguments: args,
      returnType: `${ot.name}!`,
    });
  }

  private buildUpdate(ot: GQLType): void {
    const inputAttributes: GQLAttribute[] = ot.attributes
      .filter((attr: GQLAttribute): boolean => attr.name !== '_id')
      .map(({ name, type }: GQLAttribute): GQLAttribute => ({
        name,
        type: type.includes('!')
          ? type.slice(0, -1)
          : type,
      }));

    this.view.inputs.push({
      name: `update${ot.name}Input`,
      attributes: inputAttributes,
    });

    const args: any = [
      {
        name: '_id',
        type: 'ID!',
        last: false,
      },
      {
        name: 'data',
        type: `update${ot.name}Input!`,
        last: true,
      },
    ];

    this.view.mutations.push({
      name: `update${ot.name}`,
      returnType: `${ot.name}!`,
      arguments: args,
    });
  }

  private buildDelete(ot: GQLObjectType): void {
    const args: any = [{
      name: '_id',
      type: 'ID!',
      last: true,
    }];

    this.view.mutations.push({
      name: `delete${ot.name}`,
      arguments: args,
      returnType: `${ot.name}!`,
    });
  }
}

export default GraphQLSchemaGenerator;
