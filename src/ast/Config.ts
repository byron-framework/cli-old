import Node from 'src/ast/Node';

import NodeVisitor from 'src/generators/NodeGenerator';

class Config extends Node {
  private _apiPort: number;
  private _sinkPort: number;

  constructor(apiPort: number, sinkPort: number) {
    super();

    this._apiPort = apiPort;
    this._sinkPort = sinkPort;
  }

  public get apiPort(): number {
    return this._apiPort;
  }

  public get sinkPort(): number {
    return this._sinkPort;
  }

  public accept(visitor: NodeVisitor): void {
    visitor.visitConfig(this);
  }
}

export default Config;
