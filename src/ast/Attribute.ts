import Node from 'src/ast/Node';
import ObjectType from 'src/ast/ObjectType';

import NodeVisitor from 'src/generators/NodeGenerator';

class Attribute extends Node {
  private _name: string;
  private _type: string;
  private _primaryKey: boolean;
  private _parent: ObjectType;

  constructor(name: string, type: string, parent: ObjectType, primaryKey: boolean = false) {
    super();

    this._name = name;
    this._type = type;
    this._primaryKey = primaryKey;
    this._parent = parent;
  }

  public get name(): string {
    return this._name;
  }

  public get type(): string {
    return this._type;
  }

  public get primaryKey(): boolean {
    return this._primaryKey;
  }

  public get parent(): ObjectType {
    return this._parent;
  }

  public accept(visitor: NodeVisitor): void {
    visitor.visitAttribute(this);
  }
}

export default Attribute;
