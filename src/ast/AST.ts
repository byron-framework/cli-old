import Branch from 'src/ast/Branch';
import Node from 'src/ast/Node';

import NodeVisitor from 'src/generators/NodeGenerator';

class AST extends Branch {
  private _name: string;
  private _namespace: string;

  constructor(name: string, namespace: string) {
    super();

    this._name = name;
    this._namespace = namespace;
  }

  public get name(): string {
    return this._name;
  }

  public get namespace(): string {
    return this._namespace;
  }

  public accept(visitor: NodeVisitor): void {
    this.children.forEach((n: Node) => {
      n.accept(visitor);
    });
    visitor.visitAST(this);
  }
}

export default AST;
