import Branch from 'src/ast/Branch';

import Node from 'src/ast/Node';

import NodeVisitor from 'src/generators/NodeGenerator';

class Resolver extends Branch {
  public readonly name: string;
  public readonly return: string;
  public readonly type: string;

  constructor(name: string, returnType: string, type: string) {
    super();

    this.name = name;
    this.return = returnType;
    this.type = type;
  }

  public accept(visitor: NodeVisitor): void {
    visitor.visitResolver(this);
    this.children.forEach((child: Node): void => {
      child.accept(visitor);
    });
  }
}

export default Resolver;
