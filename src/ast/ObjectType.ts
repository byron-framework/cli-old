import Branch from 'src/ast/Branch';
import Node from 'src/ast/Node';

import NodeVisitor from 'src/generators/NodeGenerator';

class ObjectType extends Branch {
  private _name: string;
  private _actions: string[];

  constructor( name: string, actions: string[] = []) {
    super();

    this._name = name;
    this._actions = actions;
  }

  get name(): string {
    return this._name;
  }

  get actions(): string[] {
    return this._actions;
  }

  public accept(visitor: NodeVisitor): void {
    visitor.visitObjectType(this);
    this.children.forEach((child: Node): void => {
      child.accept(visitor);
    });
  }
}

export default ObjectType;
