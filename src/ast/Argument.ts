import Node from 'src/ast/Node';

import Resolver from 'src/ast/Resolver';

import NodeVisitor from 'src/generators/NodeGenerator';

class Argument extends Node {
  public readonly name: string;
  public readonly type: string;
  public readonly parent: Resolver;

  constructor(name: string, type: string, parent: Resolver) {
    super();

    this.name = name;
    this.type = type;
    this.parent = parent;
  }

  public accept(visitor: NodeVisitor): void {
    visitor.visitArgument(this);
  }
}

export default Argument;
