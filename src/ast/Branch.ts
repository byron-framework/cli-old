import Node from 'src/ast/Node';

abstract class Branch extends Node {
  private _children: Node[] = [];

  public get children(): Node[] {
    return this._children;
  }

  public addChild(node: Node): void {
    this._children.push(node);
  }
}

export default Branch;
