import NodeVisitor from 'src/generators/NodeGenerator';

abstract class Node {
  public abstract accept(visitor: NodeVisitor): void;
}

export default Node;
