export interface YAMLAttribute {
  name: string;
  type: string;
  primaryKey?: boolean;
}

export interface YAMLArgument {
  name: string;
  type: string;
}

export interface YAMLResolver {
  name: string;
  return: string;
  type: string;
  args: YAMLArgument[];
}

export interface YAMLObjectType {
  name: string;
  attributes?: YAMLAttribute[];
  actions?: string[];
}

export interface YAMLConfig {
  apiPort: number;
  sinkPort: number;
}

export interface YAMLSchema {
  ObjectTypes: YAMLObjectType[];
  Resolvers: YAMLResolver[];
}

export interface YAMLByronSchema {
  name: string;
  namespace: string;
  schema: YAMLSchema;
  config?: YAMLConfig;
}
