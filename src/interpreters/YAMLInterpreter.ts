import { safeLoad } from 'js-yaml';

import FsWrapper from 'src/FsWrapper';

import Argument from 'src/ast/Argument';
import AST from 'src/ast/AST';
import Attribute from 'src/ast/Attribute';
import Config from 'src/ast/Config';
import ObjectType from 'src/ast/ObjectType';
import Resolver from 'src/ast/Resolver';

import {
  YAMLArgument,
  YAMLAttribute,
  YAMLByronSchema,
  YAMLObjectType,
  YAMLResolver,
} from 'src/interpreters/YAMLinterfaces';

class YAMLInterpreter {
  private yaml: YAMLByronSchema;
  private ast: AST;

  constructor(pathToSchema: string) {
    this.yaml =
      safeLoad(
        FsWrapper
          .instance
          .readSchema(pathToSchema),
      );

    this.ast = new AST(this.yaml.name, this.yaml.namespace);
  }

  public buildAST(): AST {
    this.buildObjectTypes();
    this.buildResolvers();
    this.buildConfig();
    return this.ast;
  }

  private buildObjectTypes(): void {
    this.yaml.schema.ObjectTypes
      .forEach((ot: YAMLObjectType): void => {
        const obj: ObjectType = new ObjectType( ot.name, ot.actions);

        if (!ot.attributes) {
          ot.attributes = [];
        }

        ot.attributes = ot.attributes
          .filter((attr: YAMLAttribute): boolean => !attr.name.match(/^_?id$/));

        const idAttr: YAMLAttribute = {
          name: '_id',
          type: 'ID!',
          primaryKey: true,
        };

        [idAttr, ...ot.attributes]
          .forEach((attr: YAMLAttribute): void => {
            obj.addChild(new Attribute(attr.name, attr.type, obj, attr.primaryKey));
          });

        this.ast.addChild(obj);
      });
  }

  private buildResolvers(): void {
    if (!this.yaml.schema.Resolvers) {
      return;
    }

    this.yaml.schema.Resolvers
      .forEach((rsl: YAMLResolver): void => {
        const resolver: Resolver = new Resolver(rsl.name, rsl.return, rsl.type);

        if (!rsl.args) {
          rsl.args = [];
        }

        rsl.args.forEach((arg: YAMLArgument): void => {
          resolver.addChild(new Argument(arg.name, arg.type, resolver));
        });

        this.ast.addChild(resolver);
      });
  }

  private buildConfig(): void {
    if (!this.yaml.config) {
      return;
    }

    this.ast.addChild(new Config(
      this.yaml.config.apiPort,
      this.yaml.config.sinkPort,
    ));
  }
}

export default YAMLInterpreter;
